const fs = require('fs-extra');
const databases = './db/user_exp.json';
class exp_man {
	constructor() {
		this.filename = databases;
		this.setup();
	}

	initialize() {
		try {
			const init_files = []
			fs.writeFileSync(this.filename, JSON.stringify(init_files), 'utf8');
		}
		catch(error) {
			console.log(error);
		}
	}

	setup() {
		if (fs.existsSync(this.filename)) {
			try {
				const data = fs.readFileSync(this.filename, 'utf8');
				const content = JSON.parse(data);
				if (!Array.isArray(content)) {
					this.initialize();
				}
			}
			catch(error) {
				this.initialize();
				console.log(error);
			}
		}
		else {
			this.initialize();
		}
	}

	load() {
		return this.exp_db;
	}

	addEXP(userID) {
		this.exp_db[userID].xp++;
	}
}

module.exports = exp_man;

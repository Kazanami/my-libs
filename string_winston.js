const Transport = require('winston-transport');
// const util = require('util');
const fs = require('fs');
const moment = require('moment-timezone');
const TIMEFORMAT = 'YYYY-MM-DD HH:mm:ss';

class CustomTransport extends Transport {
	constructor(opts) {
		super(opts);
		this.filename = opts.filename;
		this.log_level = {
			emerg: 0,
			alert: 1,
			crit: 2,
			error: 3,
			warning: 4,
			notice: 5,
			info: 6,
			debug: 7,
		};
		this.setup();
	}

	initialize() {
		try {
                        const init_param = []
			fs.writeFileSync(this.filename, JSON.stringify(init_param), 'utf8');
		}
		catch (error) {
			console.log(error);
		}
	}

	setup() {
		if (fs.existsSync(this.filename)) {
			try {
				const data = fs.readFileSync(this.filename, 'utf8');
				const content = JSON.parse(data);
				if (!Array.isArray(content)) {
					this.initialize();
				}
			}
			catch (error) {
				this.initialize();
				// console.log(error);
			}
		}
		else {
			this.initialize();
		}
	}

	readLog() {
		let data = null;
		if (fs.existsSync(this.filename)) {
			try {
				data = fs.readFileSync(this.filename, 'utf8');
			}
			catch (error) {
				console.log(error);
			}
		}
		else {
			console.log(`file not found. create ${this.filename}`);
		}
		return data;
	}
	writeLog(info) {
		const data = this.readLog();
		let arr = [];
		if (data) {
			arr = JSON.parse(data);
		}
		info.level_number = this.log_level[info.level];
		info.date = moment(moment.now()).format(TIMEFORMAT);
		arr.push(info);
		const json = JSON.stringify(arr, '\n', 4);
		try {
			fs.writeFileSync(this.filename, json, 'utf8');
		}
		catch (error) {
			console.log(error);
		}
	}

	log(info, callback) {
		setImmediate(() => {
			this.emit('logged', info);
		});

		this.writeLog(info);
		callback();
	}
}

module.exports = CustomTransport;
